﻿using GapAssesmentBackend.API.IoC;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;

namespace GapAssesmentBackend.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            UnityInstaller.Install(config);
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "InsurancePolicyRoute",
                routeTemplate: "api/insurance-policy/{id}",
                defaults: new { id = RouteParameter.Optional, controller="InsurancePolicy" }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
