﻿using GapAssesmentBackend.Domain.Managers;
using GapAssesmentBackend.Domain.Managers.Providers;
using GapAssesmentBackend.Domain.Repositories;
using GapAssesmentBackend.Domain.Repositories.Providers;
using System.Web.Http;
using Unity;
using Unity.Lifetime;

namespace GapAssesmentBackend.API.IoC
{
    public static class UnityInstaller
    {
        public static void Install(HttpConfiguration config)
        {
            var container = new UnityContainer();

            container.RegisterType<ICoverageManager, CoverageManager>(new HierarchicalLifetimeManager());

            //Registering Repositories
            container.RegisterType<IAgencyRepo, AgencyRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<IAgencyBranchRepo, AgencyBranchRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<IContractRepo, ContractRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<ICoverageRepo, CoverageRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<ICoverageLineRepo, CoverageLineRepo>(new HierarchicalLifetimeManager());
            container.RegisterType<IInsurancePolicyRepo, InsurancePolicyRepo>(new HierarchicalLifetimeManager());

            //Registering Managers
            container.RegisterType<IAgencyManager, AgencyManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IAgencyBranchManager, AgencyBranchManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IContractManager, ContractManager>(new HierarchicalLifetimeManager());
            container.RegisterType<ICoverageManager, CoverageManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IInsurancePolicyManager, InsurancePolicyManager>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);
        }
    }
}