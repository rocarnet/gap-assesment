﻿
using GAPAssesment.Common.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace GapAssesmentBackend.API.Models
{
    public static class CoverageModelFactory
    {
        public static CoverageModel Create() { return new CoverageModel(); }
    }
    public class CoverageModel : ModelBase<Guid>
    {
        [Required]
        [MaxLength(500)]
        public string Name { get; set; }
    }

    public class CoverageNewUpdateModel
    {
        [Required]
        [MaxLength(500)]
        public string Name { get; set; }
    }
}