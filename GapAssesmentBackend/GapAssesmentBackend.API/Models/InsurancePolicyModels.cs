﻿using GAPAssesment.Common.Models;
using GapAssesmentBackend.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GapAssesmentBackend.API.Models
{
    public static class InsurancePolicyModelFactory
    {
        public static InsurancePolicyModel Create() { return new InsurancePolicyModel(); }
    }


    public class CoverageLineNewUpdateModel
    {        
        public Guid CoverageTypeId { get; set; }
        public int Percentage { get; set; }
        public RiskLevel Risk { get; set; }
    }

    public class CoverageLineModel : ModelBase<Guid>
    {
        public Guid InsurancePolicyId { get; set; }        
        public Guid CoverageTypeId { get; set; }
        public int Percentage { get; set; }
        public RiskLevel Risk { get; set; }
    }
    public class InsurancePolicyModel : ModelBase<Guid>
    {
        public string Name { get; set; }

        public IEnumerable<CoverageLineModel> Coverages { get; set; }
    }

    public class InsurancePolicyNewUpdateModel
    {
        public string Name { get; set; }
    }
}