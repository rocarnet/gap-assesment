namespace GapAssesmentBackend.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameMSIdentiyTables : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AspNetRoles", newName: "Role");
            RenameTable(name: "dbo.AspNetUserRoles", newName: "UserRole");
            RenameTable(name: "dbo.AspNetUsers", newName: "User");
            RenameTable(name: "dbo.AspNetUserClaims", newName: "UserClaim");
            RenameTable(name: "dbo.AspNetUserLogins", newName: "UserLogin");
            MoveTable(name: "dbo.Role", newSchema: "Security");
            MoveTable(name: "dbo.UserRole", newSchema: "Security");
            MoveTable(name: "dbo.User", newSchema: "Security");
            MoveTable(name: "dbo.UserClaim", newSchema: "Security");
            MoveTable(name: "dbo.UserLogin", newSchema: "Security");
        }
        
        public override void Down()
        {
            MoveTable(name: "Security.UserLogin", newSchema: "dbo");
            MoveTable(name: "Security.UserClaim", newSchema: "dbo");
            MoveTable(name: "Security.User", newSchema: "dbo");
            MoveTable(name: "Security.UserRole", newSchema: "dbo");
            MoveTable(name: "Security.Role", newSchema: "dbo");
            RenameTable(name: "dbo.UserLogin", newName: "AspNetUserLogins");
            RenameTable(name: "dbo.UserClaim", newName: "AspNetUserClaims");
            RenameTable(name: "dbo.User", newName: "AspNetUsers");
            RenameTable(name: "dbo.UserRole", newName: "AspNetUserRoles");
            RenameTable(name: "dbo.Role", newName: "AspNetRoles");
        }
    }
}
