namespace GapAssesmentBackend.API.Migrations
{
    using GapAssesmentBackend.API.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Claims;

    internal sealed class Configuration : DbMigrationsConfiguration<GapAssesmentBackend.API.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GapAssesmentBackend.API.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.


            if (!context.Users.Any(u => u.UserName == "root"))
            {
                var store = new UserStore<ApplicationUser>(context);                
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "root" };

                manager.Create(user, "ChangeItAsap!");                                
            }

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
