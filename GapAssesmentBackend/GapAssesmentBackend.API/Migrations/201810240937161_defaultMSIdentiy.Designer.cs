// <auto-generated />
namespace GapAssesmentBackend.API.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class defaultMSIdentiy : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(defaultMSIdentiy));
        
        string IMigrationMetadata.Id
        {
            get { return "201810240937161_defaultMSIdentiy"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
