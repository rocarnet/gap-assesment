﻿using GapAssesmentBackend.API.Models;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Managers;
using System;

namespace GapAssesmentBackend.API.Controllers
{
    public class CoverageController : EntityApiControllerBase<Guid, Coverage, CoverageModel, CoverageNewUpdateModel>
    {
        public CoverageController(ICoverageManager manager) : base(manager)
        {
        }

        #region Factories
        protected override Coverage EntityFactory()
        {
            return CoverageFactory.Create();
        }
        protected override CoverageModel ModelFactory()
        {
            return CoverageModelFactory.Create();
        }
        #endregion

        #region Mappers
        protected override CoverageNewUpdateModel Map(Coverage entity, CoverageNewUpdateModel model)
        {
            model.Name = entity.Name;
            return model;
        }
        protected override Coverage Map(CoverageNewUpdateModel model, Coverage entity)
        {
            entity.Name = model.Name;
            return entity;
        }
        protected override CoverageModel Map(Coverage entity, CoverageModel model)
        {
            model.Id = entity.Id;
            model.Name = entity.Name;
            model.Created = entity.Created;
            return model;
        }
        protected override Coverage Map(CoverageModel model, Coverage entity)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
