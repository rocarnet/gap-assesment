﻿using GAPAssesment.Common.Entities;
using GAPAssesment.Common.Managers;
using GAPAssesment.Common.Models;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace GapAssesmentBackend.API.Controllers
{ 
    public abstract class EntityApiControllerBase<TId,TEntity,TModel, TNewUpdateModel> : ApiController
        where TEntity : EntityBase<TId>
        where TModel : ModelBase<TId>
        where TNewUpdateModel : class
    {
        protected IEntityManager<TId, TEntity> manager;

        #region Factories
        protected abstract TEntity EntityFactory();
        protected abstract TModel ModelFactory();        
        #endregion

        #region Mappers
        protected abstract TModel Map(TEntity entity, TModel model);
        protected abstract TEntity Map(TModel model, TEntity entity);

        protected abstract TNewUpdateModel Map(TEntity entity, TNewUpdateModel model);
        protected abstract TEntity Map(TNewUpdateModel model, TEntity entity);
        #endregion
        
        public EntityApiControllerBase(IEntityManager<TId, TEntity> manager)
        {         
            this.manager = manager;
        }
        
        public async Task<IHttpActionResult> GetAsync(TId id)
        {
            try
            {
                var entity = await manager.GetAsync(id);
                if (entity == null)
                    return NotFound();

                var model = this.ModelFactory();
                model = this.Map(entity, model);
                return Ok(model);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> PostAsync(TNewUpdateModel model)
        {
            try
            {
                if (model == null) return BadRequest("Request's body do not contains the expected data");

                if (!ModelState.IsValid) return BadRequest(ModelState);

                var entity = this.EntityFactory();
                entity = this.Map(model, entity);
                var id  = await manager.CreateAsync(entity);                
                return Created(Request.RequestUri, id);
            }
            catch(Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        [HttpPut]
        public async Task<IHttpActionResult> Update(TId id, TNewUpdateModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest("Request's body do not contains the expected data");

                if (!ModelState.IsValid) return BadRequest(ModelState);

                var entity = this.EntityFactory();
                entity = this.Map(model, entity);
                entity.Id = id;
                await manager.UpdateAsync(entity);
                return Ok();
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(TId id)
        {
            try
            {
                var entity = await manager.GetAsync(id);
                if (entity == null) return NotFound();

                await manager.DeleteAsync(entity);
                return Ok();
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }

        }                
    }
}