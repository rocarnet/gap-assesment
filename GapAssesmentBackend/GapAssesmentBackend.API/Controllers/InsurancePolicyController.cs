﻿using GapAssesmentBackend.API.Models;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace GapAssesmentBackend.API.Controllers
{
    //TODO: uncomment this when login endpoint be available.
    //[Authorize]
    public class InsurancePolicyController : EntityApiControllerBase<Guid, InsurancePolicy, InsurancePolicyModel, InsurancePolicyNewUpdateModel>
    {
        private IInsurancePolicyManager insurancePolicyManager;
        public InsurancePolicyController(IInsurancePolicyManager manager) : base(manager)
        {
            this.insurancePolicyManager = manager;
        }

        #region Factories
        protected override InsurancePolicy EntityFactory()
        {
            return InsurancePolicyFactory.Create();
        }
        protected override InsurancePolicyModel ModelFactory()
        {
            return InsurancePolicyModelFactory.Create();
        }
        #endregion

        #region Mappers
        protected override InsurancePolicyModel Map(InsurancePolicy entity, InsurancePolicyModel model)
        {
            model.Id = entity.Id;
            model.Created = entity.Created;
            model.Name = entity.Name;
            if (entity.Coverages.Exists())
                model.Coverages = entity.Coverages.Select(line => new CoverageLineModel
                {
                    Id = line.Id,
                    Created = line.Created,
                    CoverageTypeId = line.CoverageTypeId,
                    Percentage = line.Percentage,
                    Risk = line.Risk
                }).ToList();
            return model;
        }
        protected override InsurancePolicy Map(InsurancePolicyModel model, InsurancePolicy entity)
        {
            entity.Id = model.Id;
            entity.Created = model.Created;
            entity.Name = model.Name;
            if (model.Coverages.Exists())
                entity.Coverages = model.Coverages.Select(line => new CoverageLine
                {
                    Id = line.Id,
                    Created = line.Created,
                    CoverageTypeId = line.CoverageTypeId,
                    Percentage = line.Percentage,
                    Risk = line.Risk
                }).ToList();
            return entity;
        }
        protected override InsurancePolicyNewUpdateModel Map(InsurancePolicy entity, InsurancePolicyNewUpdateModel model)
        {
            model.Name = entity.Name;
            return model;
        }
        protected override InsurancePolicy Map(InsurancePolicyNewUpdateModel model, InsurancePolicy entity)
        {
            entity.Name = model.Name;
            return entity;
        }
        #endregion

        private CoverageLine Map(CoverageLineNewUpdateModel model)
        {
            var coverageLine = CoverageLineFactory.Create();
            coverageLine.CoverageTypeId = model.CoverageTypeId;
            coverageLine.Percentage = model.Percentage;
            coverageLine.Risk = model.Risk;
            return coverageLine;
        }

        [HttpPost]
        [Route("api/insurance-policy/{id}/coverage-lines")]
        public async Task<IHttpActionResult> AddCoverageLine(Guid id, CoverageLineNewUpdateModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest("Request body do not contains the expected data");

                var coverageLine = Map(model);
                Guid? coverageLineId = await insurancePolicyManager.AddCoverageLine(id, coverageLine);
                return Created(Request.RequestUri, coverageLineId);
            }
            catch (ValidationException validationError)
            {
                return BadRequest(validationError.Message);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }

        [HttpDelete]
        [Route("api/insurance-policy/{id}/coverage-lines/{coverageLineId}")]
        public async Task<IHttpActionResult> RemoveCoverageLine(Guid id, Guid coverageLineId)
        {
            try
            {
                await insurancePolicyManager.RemoveCoverageLine(id, coverageLineId);
                return Ok();
            }
            catch (ValidationException validationError)
            {
                return BadRequest(validationError.Message);
            }
            catch (Exception exception)
            {
                return InternalServerError(exception);
            }
        }
    }
}
