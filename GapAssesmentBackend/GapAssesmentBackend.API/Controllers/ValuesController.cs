﻿using GapAssesmentBackend.API.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace GapAssesmentBackend.API.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            var objectToserialize = new CoverageLineNewUpdateModel
            {
                CoverageTypeId = Guid.Parse("D74D6548-5576-4141-B754-89D076E6A984"),
                Percentage =50,
                Risk = Domain.Entities.RiskLevel.Medium
            };

            var xml = new XmlMediaTypeFormatter();
            string str = Serialize(xml, objectToserialize);

            var json = new JsonMediaTypeFormatter();
            str = Serialize(json, objectToserialize);

            // Round trip
            var originalObject = Deserialize<CoverageLineNewUpdateModel>(json, str);

            return new string[] { "value1", "value2" };
        }


        string Serialize<T>(MediaTypeFormatter formatter, T value)
        {
            // Create a dummy HTTP Content.
            Stream stream = new MemoryStream();
            var content = new StreamContent(stream);
            /// Serialize the object.
            formatter.WriteToStreamAsync(typeof(T), value, stream, content, null).Wait();
            // Read the serialized string.
            stream.Position = 0;
            return content.ReadAsStringAsync().Result;
        }

        T Deserialize<T>(MediaTypeFormatter formatter, string str) where T : class
        {
            // Write the serialized string to a memory stream.
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            // Deserialize to an object of type T
            return formatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
        }
    }
}
