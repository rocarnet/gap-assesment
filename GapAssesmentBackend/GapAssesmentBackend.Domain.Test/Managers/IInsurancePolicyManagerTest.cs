﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Managers;
using GapAssesmentBackend.Domain.Managers.Providers;
using GapAssesmentBackend.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GapAssesmentBackend.Domain.Test.Managers
{
    [TestClass]
    public class InsurancePolicyManagerTest
    {
        public TestContext TestContext { get; set; }

        public readonly List<InsurancePolicy> insuancePolicies;
        public readonly List<CoverageLine> coveraageLines;
        public readonly IInsurancePolicyRepo insurancePolicyRepo;
        public readonly ICoverageLineRepo coverageLineRepo;
        public readonly IInsurancePolicyManager manager;

        public InsurancePolicyManagerTest()
        {
            insuancePolicies = new List<InsurancePolicy>()
            {
                new InsurancePolicy
                {
                    Id = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002"),
                    Name =  "Serguro de Vehiculo todo riesgo",
                    Created = DateTime.Now,
                    Active = true
                },
                new InsurancePolicy
                {
                    Id = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002"),
                    Name =  "Serguro deudor financiero",
                    Created = DateTime.Now,
                    Active = true
                }
            };
            coveraageLines = new List<CoverageLine>()
            {
                new CoverageLine
                {
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000001"),
                    Active = true,
                    Created = DateTime.Now,
                    CoverageTypeId = Guid.Parse("C0000000-0000-0000-0000-0000000000B1"),
                    InsurancePolicyId = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0001"),
                    Percentage = 15,
                    Risk = RiskLevel.Medium
                },
                new CoverageLine
                {
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000002"),
                    Active = true,
                    Created = DateTime.Now,
                    CoverageTypeId = Guid.Parse("C0000000-0000-0000-0000-0000000000B2"),
                    InsurancePolicyId = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0001"),
                    Percentage = 65,
                    Risk = RiskLevel.MediumHigh
                },
                new CoverageLine
                {
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000003"),
                    Active = true,
                    Created = DateTime.Now,
                    CoverageTypeId = Guid.Parse("C0000000-0000-0000-0000-0000000000B1"),
                    InsurancePolicyId = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002"),
                    Percentage = 15,
                    Risk = RiskLevel.Medium
                },
                new CoverageLine
                {
                    Id = Guid.Parse("00000000-0000-0000-0000-000000000004"),
                    Active = true,
                    Created = DateTime.Now,
                    CoverageTypeId = Guid.Parse("C0000000-0000-0000-0000-0000000000B2"),
                    InsurancePolicyId = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002"),
                    Percentage = 60,
                    Risk = RiskLevel.Low
                },
            };

            var mockInsurancePolicyRepo = new Mock<IInsurancePolicyRepo>();
            var mockCoverageLineRepo = new Mock<ICoverageLineRepo>();

            mockInsurancePolicyRepo.Setup(operation => operation.GetAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(insuancePolicies.FirstOrDefault(ip => ip.Id == Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002"))));

            mockCoverageLineRepo.Setup(operation => operation.GetByInsurancePolicy(It.IsAny<Guid>()))
                .Returns(Task.FromResult(coveraageLines.Where(cl => cl.InsurancePolicyId == Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002"))));

            insurancePolicyRepo = mockInsurancePolicyRepo.Object;
            coverageLineRepo = mockCoverageLineRepo.Object;
            manager = new InsurancePolicyManager(insurancePolicyRepo, coverageLineRepo);
        }

        [TestMethod]
        public async void GetById()
        {
            //Arrange
            var insurancePolicyId = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002");
            var insurancePolicy = insuancePolicies.First(ip => ip.Id == insurancePolicyId);

            //Act            
            var result = await manager.GetAsync(insurancePolicyId);
            
            //Assert
            Assert.AreEqual(result, insurancePolicy);
        }

        [TestMethod]
        public async void AddInvalidCoverateLineToInsurancePolicy()
        {

            //Arrange
            var insurancePolicyId = Guid.Parse("AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAA0002");
            var newLine = new CoverageLine
            {
                //InsurancePolicyId = insurancePolicyId,
                Id = Guid.NewGuid(),
                Percentage = 60,
                Risk = RiskLevel.High
            };
                        
            //Act            
            var result = await manager.AddCoverageLine(insurancePolicyId, newLine);

            //Assert
            Assert.IsNull(result);

        }
    }
}
