﻿using GAPAssesment.Common.Managers;
using GapAssesmentBackend.Domain.Entities;
using System;

namespace GapAssesmentBackend.Domain.Managers
{
    public interface IAgencyManager : IEntityManager<Guid, Agency>
    {
    }
}
