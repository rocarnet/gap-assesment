﻿using GAPAssesment.Common.Managers;
using GapAssesmentBackend.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace GapAssesmentBackend.Domain.Managers
{
    public interface IInsurancePolicyManager : IEntityManager<Guid, InsurancePolicy>
    {
        bool IncludeCoverages { get; set; }

        Task<Guid?> AddCoverageLine(Guid insurancePolicyId, CoverageLine coverageLine);
        Task RemoveCoverageLine(Guid  insurancePolicyId, Guid coverageLineId);
    }
}