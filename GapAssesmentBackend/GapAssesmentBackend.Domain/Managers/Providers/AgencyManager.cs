﻿using GAPAssesment.Common.Managers;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Repositories;
using System;

namespace GapAssesmentBackend.Domain.Managers.Providers
{
    public class AgencyManager : EntityManagerBase<Guid, Agency>, IAgencyManager
    {
        protected IAgencyRepo agencyRepo;
        public AgencyManager(IAgencyRepo agencyRepo) : base(agencyRepo)
        {
        }
    }
}
