﻿using GAPAssesment.Common.Managers;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Repositories;
using System;

namespace GapAssesmentBackend.Domain.Managers.Providers
{
    public class CoverageManager : EntityManagerBase<Guid, Coverage>, ICoverageManager
    {
        protected ICoverageRepo coverageRepo;
        public CoverageManager(ICoverageRepo coverageRepo) :base(coverageRepo)
        {
        }
    }
}
