﻿using GAPAssesment.Common.Managers;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Repositories;
using System;

namespace GapAssesmentBackend.Domain.Managers.Providers
{
    public class ContractManager: EntityManagerBase<Guid, Contract>, IContractManager
    {
        protected IContractRepo contractRepo;
        public ContractManager(IContractRepo contractRepo) : base(contractRepo)
        {
        }
    }
}
