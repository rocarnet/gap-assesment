﻿using GAPAssesment.Common.Managers;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Repositories;
using System;

namespace GapAssesmentBackend.Domain.Managers.Providers
{
    public class AgencyBranchManager : EntityManagerBase<Guid, AgencyBranch>, IAgencyBranchManager
    {
        protected IAgencyBranchRepo agencyBranchRepo;
        public AgencyBranchManager(IAgencyBranchRepo agencyBranchRepo) : base(agencyBranchRepo)
        {
        }
    }
}
