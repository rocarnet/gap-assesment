﻿using GAPAssesment.Common.Managers;
using GapAssesmentBackend.Domain.Entities;
using GapAssesmentBackend.Domain.Repositories;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GapAssesmentBackend.Domain.Managers.Providers
{
    public class InsurancePolicyManager : EntityManagerBase<Guid, InsurancePolicy>, IInsurancePolicyManager
    {
        protected ICoverageLineRepo coverageLineRepo;
        private IInsurancePolicyRepo insurancePolicyRepo;
        public InsurancePolicyManager(IInsurancePolicyRepo insurancePolicyRepo, ICoverageLineRepo coverageLineRepo) : base(insurancePolicyRepo)
        {
            this.coverageLineRepo = coverageLineRepo;
            this.insurancePolicyRepo = insurancePolicyRepo;
        }

        public bool IncludeCoverages { get; set; }

        public override async Task<InsurancePolicy> GetAsync(Guid id)
        {
            var insurancePolicy = await base.GetAsync(id);
            if (this.IncludeCoverages)
                insurancePolicy.Coverages = await coverageLineRepo.Query().Where(line => line.InsurancePolicyId == insurancePolicy.Id && line.Active).ToListAsync();
            return insurancePolicy;
        }

        //A more elegant way to do this validation is creating a custom validation attribute inheriting from CustomValidationAttribute.    
        //TODO: Move validation messages to a resource file
        private async Task<ValidationException> ValidateInsurancePolicyExistence(Guid insurancePolicyId)
        {
            var insurancePolicy = await insurancePolicyRepo.GetAsync(insurancePolicyId);
            if (insurancePolicy == null)
                return new ValidationException(string.Format("There is not an insurance policy with Id = '{0}'", insurancePolicyId));
            return null;
        }

        //TODO: Move validation messages to a resource file
        //TODO: change this validation to be done in an validation attribute class.
        private async Task<ValidationException> ValidateCoverageRiskRule(Guid insurancePolicyId, CoverageLine coverageLine, bool create = true)
        {
            if (coverageLine == null) throw new ArgumentNullException("coverageLine");

            const string cannotAddInvalidLineMessage = "Cannot add this coverage because its risk is high and its percentage of coverage is higher than 50%";
            const string cannotUpdateWithInvalidLine = "Cannot set a coverage percentage higher than 50% because its risk level is high";
            const string percentageNotValidForThisInsurancePolicy = "This insurance policy does not admits coverage percentages higher to 50%";
            const string insurancePolicyDoesNotAdmitHighRisk = "This insurance policy cannot accept a High Risk coverage";

            if (coverageLine.Percentage > 50 && coverageLine.Risk == RiskLevel.High)
                return new ValidationException((create) ? cannotAddInvalidLineMessage: cannotUpdateWithInvalidLine);

            var coverageLines = await coverageLineRepo.GetByInsurancePolicy(insurancePolicyId);

            var containsCoverageHigerThan50 = coverageLines != null && coverageLines.Any(line => line.Percentage > 50 && line.Id != coverageLine.Id);
            var containsHighRisk = coverageLines != null && coverageLines.Any(line => line.Risk == RiskLevel.High && line.Id != coverageLine.Id);


            if (containsCoverageHigerThan50 && coverageLine.Risk == RiskLevel.High)
            {
                return new ValidationException(insurancePolicyDoesNotAdmitHighRisk);
            }
            
            if (containsHighRisk && coverageLine.Percentage > 50)
            {
                return new ValidationException((create) ? percentageNotValidForThisInsurancePolicy : cannotUpdateWithInvalidLine);
            }
                                              
            return null;
        }


        public async Task<Guid?> AddCoverageLine(Guid insurancePolicyId, CoverageLine coverageLine)
        {
            Guid? coverageLineId = null;

            var validationException = await ValidateInsurancePolicyExistence(insurancePolicyId);
            if (validationException != null) throw validationException;

            validationException = await ValidateCoverageRiskRule(insurancePolicyId, coverageLine);
            if (validationException != null) throw validationException;

            coverageLine.InitializaAsNewInstance();
            coverageLine.InsurancePolicyId = insurancePolicyId;
            coverageLineId = await coverageLineRepo.InsertAsync(coverageLine);
            return coverageLineId;
        }
        public async Task RemoveCoverageLine(Guid insurancePolicyId, Guid coverageLineId)
        {
            var validationException = await ValidateInsurancePolicyExistence(insurancePolicyId);
            if (validationException != null) throw validationException;

            var coverageLine = await coverageLineRepo.GetAsync(coverageLineId);
            if (insurancePolicyId != coverageLine.InsurancePolicyId)
                throw new ValidationException("The coverage to be deleted does not belongs to the insurance policy specified.");
            
            coverageLine.Active = false;
            await coverageLineRepo.UpdateAsync(coverageLine);            
        }        
    }
}
