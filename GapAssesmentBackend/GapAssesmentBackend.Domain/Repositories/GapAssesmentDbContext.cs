﻿using GapAssesmentBackend.Domain.Entities;
using System.Data.Entity;

namespace GapAssesmentBackend.Domain.Repositories
{
    public class GapAssesmentDbContext : DbContext
    {
        static GapAssesmentDbContext()
        {
            Database.SetInitializer<GapAssesmentDbContext>(null);
        }

        public GapAssesmentDbContext() : base("name=DefaultConnection")
        {
            Database.SetInitializer<GapAssesmentDbContext>(null);
        }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Agency>().ToTable("Agency", "Sales");
            modelBuilder.Entity<AgencyBranch>().ToTable("AgencyAgencyBranch", "Sales");
            
            modelBuilder.Entity<Coverage>().ToTable("Coverage", "InsurancePolicy");
            modelBuilder.Entity<CoverageLine>().ToTable("CoverageLine", "InsurancePolicy");
            modelBuilder.Entity<InsurancePolicy>().ToTable("InsurancePolicy", "InsurancePolicy");

            modelBuilder.Entity<Contract>().ToTable("Contract", "InsurancePolicy");
        }

    }
}
