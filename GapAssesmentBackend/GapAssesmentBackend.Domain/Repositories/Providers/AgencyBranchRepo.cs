﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;

namespace GapAssesmentBackend.Domain.Repositories.Providers
{
    public class AgencyBranchRepo : RepositoryBase<Guid, AgencyBranch>, IAgencyBranchRepo
    {
        public AgencyBranchRepo(GapAssesmentDbContext context) : base(context)
        {
        }
    }
}
