﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;

namespace GapAssesmentBackend.Domain.Repositories.Providers
{
    public class AgencyRepo : RepositoryBase<Guid,Agency>, IAgencyRepo
    {
        public AgencyRepo(GapAssesmentDbContext dbContext) : base(dbContext)
        {
        }
    }
}
