﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;

namespace GapAssesmentBackend.Domain.Repositories.Providers
{
    public class InsurancePolicyRepo : RepositoryBase<Guid, InsurancePolicy>, IInsurancePolicyRepo
    {
        public InsurancePolicyRepo(GapAssesmentDbContext dbContext) : base(dbContext)
        {
        }
    }
}
