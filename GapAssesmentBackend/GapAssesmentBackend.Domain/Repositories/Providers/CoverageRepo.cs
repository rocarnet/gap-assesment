﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;

namespace GapAssesmentBackend.Domain.Repositories.Providers
{
    public class CoverageRepo : RepositoryBase<Guid, Coverage>, ICoverageRepo
    {
        public CoverageRepo(GapAssesmentDbContext dbContext) : base(dbContext)
        {
        }
    }
}
