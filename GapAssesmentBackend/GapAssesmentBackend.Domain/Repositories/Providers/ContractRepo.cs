﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;

namespace GapAssesmentBackend.Domain.Repositories.Providers
{
    public class ContractRepo : RepositoryBase<Guid, Contract>, IContractRepo
    {
        public ContractRepo(GapAssesmentDbContext dbContext) : base(dbContext)
        {
        }
    }
}
