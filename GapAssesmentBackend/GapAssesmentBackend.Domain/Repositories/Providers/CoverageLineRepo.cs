﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GapAssesmentBackend.Domain.Repositories.Providers
{
    public class CoverageLineRepo : RepositoryBase<Guid, CoverageLine>, ICoverageLineRepo
    {
        public CoverageLineRepo(GapAssesmentDbContext dbContext) : base(dbContext)
        {            
        }
        public async Task<IEnumerable<CoverageLine>> GetByInsurancePolicy(Guid insurancePolicyId)
        {
            return await this.Query().Where(line => line.InsurancePolicyId == insurancePolicyId && line.Active).ToListAsync();
        }
    }
}
