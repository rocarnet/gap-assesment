﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;

namespace GapAssesmentBackend.Domain.Repositories
{
    public interface IAgencyBranchRepo : IRepository<Guid, AgencyBranch>
    {
    }
}
