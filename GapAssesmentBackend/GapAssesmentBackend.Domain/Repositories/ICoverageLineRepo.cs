﻿using GAPAssesment.Common.Persistence;
using GapAssesmentBackend.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GapAssesmentBackend.Domain.Repositories
{
    public interface ICoverageLineRepo : IRepository<Guid, CoverageLine>
    {
        Task<IEnumerable<CoverageLine>> GetByInsurancePolicy(Guid insurancePolicyId);
    }
}
