namespace GapAssesmentBackend.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Sales.Agency",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Code = c.String(),
                        SiteUrl = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        Created = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Sales.AgencyAgencyBranch",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AgencyId = c.Guid(nullable: false),
                        Address = c.String(),
                        City = c.String(),
                        Phone = c.String(),
                        Fax = c.String(),
                        Created = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Sales.Agency", t => t.AgencyId, cascadeDelete: true)
                .Index(t => t.AgencyId);
            
            CreateTable(
                "InsurancePolicy.Coverage",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Created = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "InsurancePolicy.CoverageLine",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        InsurancePolicyId = c.Guid(nullable: false),
                        CoverageTypeId = c.Guid(nullable: false),
                        Percentage = c.Int(nullable: false),
                        Risk = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("InsurancePolicy.Coverage", t => t.CoverageTypeId, cascadeDelete: true)
                .ForeignKey("InsurancePolicy.InsurancePolicy", t => t.InsurancePolicyId, cascadeDelete: true)
                .Index(t => t.InsurancePolicyId)
                .Index(t => t.CoverageTypeId);
            
            CreateTable(
                "InsurancePolicy.InsurancePolicy",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Created = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "InsurancePolicy.Contract",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClientName = c.String(),
                        EffectiveDate = c.DateTime(nullable: false),
                        CoveragePeriod = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        InsurancePolicyId = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("InsurancePolicy.InsurancePolicy", t => t.InsurancePolicyId, cascadeDelete: true)
                .Index(t => t.InsurancePolicyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("InsurancePolicy.Contract", "InsurancePolicyId", "InsurancePolicy.InsurancePolicy");
            DropForeignKey("InsurancePolicy.CoverageLine", "InsurancePolicyId", "InsurancePolicy.InsurancePolicy");
            DropForeignKey("InsurancePolicy.CoverageLine", "CoverageTypeId", "InsurancePolicy.Coverage");
            DropForeignKey("Sales.AgencyAgencyBranch", "AgencyId", "Sales.Agency");
            DropIndex("InsurancePolicy.Contract", new[] { "InsurancePolicyId" });
            DropIndex("InsurancePolicy.CoverageLine", new[] { "CoverageTypeId" });
            DropIndex("InsurancePolicy.CoverageLine", new[] { "InsurancePolicyId" });
            DropIndex("Sales.AgencyAgencyBranch", new[] { "AgencyId" });
            DropTable("InsurancePolicy.Contract");
            DropTable("InsurancePolicy.InsurancePolicy");
            DropTable("InsurancePolicy.CoverageLine");
            DropTable("InsurancePolicy.Coverage");
            DropTable("Sales.AgencyAgencyBranch");
            DropTable("Sales.Agency");
        }
    }
}
