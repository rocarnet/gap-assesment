﻿using GAPAssesment.Common.Entities;
using System;
using System.Collections.Generic;

namespace GapAssesmentBackend.Domain.Entities
{
    public static class InsurancePolicyFactory
    {
        public static InsurancePolicy Create() { return new InsurancePolicy(); }
    }
    public class InsurancePolicy: EntityBase<Guid>
    {
        public string Name { get; set; }
        public IEnumerable<CoverageLine> Coverages { get; set; }

        public override Guid GetNewId()
        {
            return Guid.NewGuid();
        }        
    }
}
