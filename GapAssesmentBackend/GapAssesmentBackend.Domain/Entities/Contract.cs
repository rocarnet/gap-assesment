﻿using GAPAssesment.Common.Entities;
using System;

namespace GapAssesmentBackend.Domain.Entities
{
    public static class ContractFactory
    {
        public static Contract Create() { return new Contract(); }
    }
    public class Contract : EntityBase<Guid>
    {
        public string ClientName { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int CoveragePeriod { get; set; } //In Months
        public double Price { get; set; }

        public Guid InsurancePolicyId { get; set; }
        public InsurancePolicy InsurancePolicy { get; set; }

        public override Guid GetNewId()
        {
            return Guid.NewGuid();
        }        
    }
}
