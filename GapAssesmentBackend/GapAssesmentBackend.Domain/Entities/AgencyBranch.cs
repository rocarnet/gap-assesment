﻿using GAPAssesment.Common.Entities;
using System;

namespace GapAssesmentBackend.Domain.Entities
{
    public static class AgencyBranchFactory
    {
        public static AgencyBranch Create() { return new AgencyBranch(); }
    }
    public class AgencyBranch : EntityBase<Guid>
    {
        public Guid AgencyId { get; set; }
        public Agency Agency { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public override Guid GetNewId()
        {
            return Guid.NewGuid();
        }        
    }
}
