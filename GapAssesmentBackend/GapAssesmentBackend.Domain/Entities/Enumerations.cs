﻿namespace GapAssesmentBackend.Domain.Entities
{
    public enum RiskLevel
    {
        Low,
        Medium,
        MediumHigh,
        High
    }
}
