﻿using GAPAssesment.Common.Entities;
using System;

namespace GapAssesmentBackend.Domain.Entities
{
    public static class AgencyFactory
    {
        public static Agency Create() { return new Agency(); }
    }
    public class Agency : EntityBase<Guid>
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public String SiteUrl { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public override Guid GetNewId()
        {
            return Guid.NewGuid();
        }        
    }
}
