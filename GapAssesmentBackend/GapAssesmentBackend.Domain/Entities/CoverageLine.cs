﻿using GAPAssesment.Common.Entities;
using System;

namespace GapAssesmentBackend.Domain.Entities
{
    public static class CoverageLineFactory
    {
        public static CoverageLine Create() { return new CoverageLine(); }
    }
    public class CoverageLine : EntityBase<Guid> 
    {
        public Guid InsurancePolicyId { get; set; }
        public InsurancePolicy InsurancePolicy { get; set; }

        public Guid CoverageTypeId { get; set; }
        public Coverage CoverageType { get; set; }
        public int Percentage { get; set; }
        public RiskLevel Risk { get; set; }

        public override Guid GetNewId()
        {
            return Guid.NewGuid();
        }
    }
}
