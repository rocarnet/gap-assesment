﻿using GAPAssesment.Common.Entities;
using System;

namespace GapAssesmentBackend.Domain.Entities
{
    public static class CoverageFactory
    {
        public static Coverage Create() { return new Coverage(); }
    }

    public class Coverage : EntityBase<Guid>
    {        
        public string Name { get; set; }
        public override Guid GetNewId()
        {
            return Guid.NewGuid();
        }        
    }
}
