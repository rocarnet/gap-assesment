﻿using System;

namespace GAPAssesment.Common.Entities
{
    public abstract class EntityBase<T>
    {
        public T Id { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public bool Active { get; set; } = true;

        public abstract T GetNewId();               

        public void  InitializaAsNewInstance()
        {
            this.Id = GetNewId();
            this.Created = DateTime.Now;
            this.Active = true;
        }

        public void MarkAsDeleted()
        {
            this.Active = false;
        }        
    }
}
