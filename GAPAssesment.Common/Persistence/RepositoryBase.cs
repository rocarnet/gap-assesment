﻿using GAPAssesment.Common.Entities;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GAPAssesment.Common.Persistence
{
    public abstract class RepositoryBase<TId, TEntity> : IRepository<TId, TEntity>
        where TEntity : EntityBase<TId>
    {
        public RepositoryBase(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        protected DbContext dbContext;
        #region RepositoryBase<TId, TEntity, TDbContext> members

        public IQueryable<TEntity> Query()
        {
            return dbContext.Set<TEntity>().AsQueryable();
        }
        public async Task<TEntity> GetAsync(TId id)
        {
            var entities = dbContext.Set<TEntity>().AsQueryable();
            return await entities.FirstOrDefaultAsync<TEntity>(entity => id.ToString().Equals(entity.Id.ToString())&& entity.Active);
        }
        public async Task<TId> InsertAsync(TEntity entity)
        {
            entity.Id = entity.GetNewId();
            dbContext.Set<TEntity>().Add(entity);
            await dbContext.SaveChangesAsync();
            return entity.Id;
        }
        public async Task UpdateAsync(TEntity entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
            await dbContext.SaveChangesAsync();            
        }

        #endregion
    }
}
