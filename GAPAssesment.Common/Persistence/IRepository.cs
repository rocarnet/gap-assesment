﻿using GAPAssesment.Common.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace GAPAssesment.Common.Persistence
{
    public interface IRepository<TId, TEntity>
        where TEntity : EntityBase<TId>        
    {
        IQueryable<TEntity> Query();
        Task<TEntity> GetAsync(TId id);
        Task<TId> InsertAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);        
    }
}
