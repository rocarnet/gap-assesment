﻿using GAPAssesment.Common.Entities;
using System.Threading.Tasks;

namespace GAPAssesment.Common.Managers
{
    public interface IEntityManager<TId, TEntity>
        where TEntity : EntityBase<TId>
    {
        Task<TEntity> GetAsync(TId id);
        Task<TId> CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
    }
}
