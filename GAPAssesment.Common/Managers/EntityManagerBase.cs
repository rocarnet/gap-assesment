﻿using GAPAssesment.Common.Entities;
using GAPAssesment.Common.Persistence;
using System.Threading.Tasks;

namespace GAPAssesment.Common.Managers
{
    public abstract class EntityManagerBase<TId, TEntity>         
        : IEntityManager<TId, TEntity>
        where TEntity : EntityBase<TId>
    {
        protected IRepository<TId, TEntity> repository;
        public EntityManagerBase(IRepository<TId, TEntity> repository)
        {
            this.repository = repository;
        }
        public virtual async Task<TId> CreateAsync(TEntity entity)
        {
            entity.InitializaAsNewInstance();          
            var id  = await repository.InsertAsync(entity);
            return id;
        }
        public virtual async Task UpdateAsync(TEntity entity)
        {            
            await repository.UpdateAsync(entity);
        }
        public virtual async Task DeleteAsync(TEntity entity)
        {
            entity.MarkAsDeleted();
            await repository.UpdateAsync(entity);
        }        
        public virtual async Task<TEntity> GetAsync(TId id)
        {
           return await repository.GetAsync(id);
        }
    }
}
