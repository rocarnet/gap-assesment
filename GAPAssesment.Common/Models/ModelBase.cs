﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GAPAssesment.Common.Models
{
    public abstract class ModelBase<TId>
    {
        [Required]
        public TId Id { get; set; } 
        public DateTime Created { get; set; }
    }
}
