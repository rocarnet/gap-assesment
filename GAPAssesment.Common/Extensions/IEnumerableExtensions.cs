﻿using System.Linq;

namespace System.Collections.Generic
{
    public static class IEnumerableExtensions
    {
        public static bool Exists<T>(this IEnumerable<T> enumerable)
        {
            return (enumerable != null && enumerable.Any());
        }
    }
}
